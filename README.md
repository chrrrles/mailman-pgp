# mailman-pgp
[![PyPI](https://img.shields.io/pypi/v/mailman-pgp.svg)](https://pypi.org/project/mailman-pgp/) [![build status](https://gitlab.com/J08nY/mailman-pgp/badges/master/build.svg)](https://gitlab.com/J08nY/mailman-pgp/commits/master) [![coverage report](https://gitlab.com/J08nY/mailman-pgp/badges/master/coverage.svg)](https://gitlab.com/J08nY/mailman-pgp/commits/master) [![docs](https://readthedocs.org/projects/mailman-pgp/badge/?version=latest)](http://mailman-pgp.readthedocs.io/en/latest/?badge=latest)

A plugin for GNU Mailman that adds encrypted mailing lists via PGP/MIME.

## Installation

Simply install into the same environment as Mailman Core and follow configuration steps to enable. For example using:
```
pip install mailman-pgp
```
For more, see [install.rst](docs/install.rst)

## Configuration

See [mailman.cfg](src/mailman_pgp/config/mailman.cfg) for example configuration which enables the mailman-pgp plugin in Mailman Core and sets up the plugin configuration to the example [mailman_pgp.cfg](src/mailman_pgp/config/mailman_pgp.cfg).
For more, see [config.rst](docs/config.rst).

## License
    
    Copyright (C) 2017 Jan Jancar
    This program is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.
    
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.
    
    You should have received a copy of the GNU General Public License along with
    this program.  If not, see <http://www.gnu.org/licenses/>.