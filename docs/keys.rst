====
Keys
====

Mailman-pgp stores PGP keys in ASCII-Armored files in directories.
The ``[keydirs]`` section of the config specifies three directories, one for list
keypairs, one for user public keys(per-address) and one for list archive public keys.

List keypairs
=============

Mailman-pgp can be configured to generate the list keypair on PGP enabled list creation.
This is done via the ``[keypairs].autogenerate`` option.

The type and size of the key and subkey that is generated is also configurable,
via the ``[keypairs].primary_key`` and ``.sub_key`` options. The options are listed
in the `src/mailman_pgp/config/mailman_pgp.cfg` config file and also in
the :doc:`config` docs.

Mailman-pgp generates keys that look like so::

    pub   secp256k1/0x651AD9483EB388DD 2017-08-21 [SC]
          Key fingerprint = 2767 BEE0 E502 00DA 4A2F  131E 651A D948 3EB3 88DD
    uid                             Name <name@example.com>
    uid                             Name <name-request@example.com>
    sub   secp256k1/0x5D972B21F6D1C7D7 2017-08-21 [E]


For a list called ``name`` at the ``example.com`` domain, with the ``ECDSA`` and ``ECDH`` algorithms over secp256k1
, respectively.

As the list keypair is stored as an ASCII-Armored text file in the ``[keydirs].list_keydir``
directory, named ``<list-id>.asc`` , replacing it with a custom keypair just works. However, doing so to
an established mailing list with subscribers, might confuse them, and if they
still encrypt to the old key, mailman-pgp will bounce their messages.

Mailman-pgp can delete and or shred the list keypair on list deletion, this
is also configured in the ``[keypairs]`` config section, via ``shred``, ``shred_command`` and
``delete`` options.