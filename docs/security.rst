=======================
Security considerations
=======================

Mailman-pgp needs to process the messages when they arrive to the mailing list,
to do so it has to decrypt them. Then they pass through Mailman chains and
pipelines to be (optionally) encrypted again and sent out.

Keys are currently stored not encrypted.

Mailman-pgp only provides some confirmation that the subscriber has access to
the signing capability of the key provided on subscription, by requesting the
user to sign a statement saying so. It is up to the list moderator/admin to
verify and confirm the subscribers identity.

Any successful subscriber that has his key set, will receive messages encrypted
to his key(if the mailing list is set to encrypt) and thus even one compromised
or malicious subscriber will compromise all messages of a mailing list.

