============
Installation
============

Mailman-pgp currently depends on a development version of both `Mailman <https://gitlab.com/mailman/mailman>`_ and `PGPy <https://github.com/SecurityInnovation/PGPy>`_.
However, the required changes are slowly getting merged into both. Until they are fully merged, doing::

    pip install mailman-pgp

pulls in those development versions from `J08nY/mailman/plugin <https://gitlab.com/J08nY/mailman/tree/plugin>`_, `J08nY/PGPy/dev <https://github.com/J08nY/PGPy/tree/dev>`_.

See :doc:`config` for configuration which enables the mailman-pgp plugin after installation.