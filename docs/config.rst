=============
Configuration
=============

To enable and configure the mailman-pgp plugin, both Mailman Core needs to be instructed to find the correct plugin
package and class to find mailman-pgp and mailman-pgp needs to be configured to work correctly.

Mailman
=======

Example additions to mailman.cfg to enable mailman-pgp:

.. literalinclude:: ../src/mailman_pgp/config/mailman.cfg


Plugin
======

Default PGP config:

.. literalinclude:: ../src/mailman_pgp/config/mailman_pgp.cfg
